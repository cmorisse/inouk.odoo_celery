from __future__ import absolute_import, unicode_literals
import sys
sys.path[0:0] = [
  '/opt/openerp/xayoni/appserver-xy/eggs/pyPdf-1.13-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/ofxparse-0.15-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/inouk.edofx-0.3.8-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/setuptools-34.2.0-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/google_api_python_client-1.6.2-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/Wand-0.4.2-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/python_slugify-1.1.4-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/minecart-0.2.1-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/pdfminer-20140328-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/pytesseract-0.1.6-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/PyPDF2-1.25.1-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/a.r.odoo',
  '/opt/openerp/xayoni/appserver-xy/eggs/Babel-2.3.4-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/decorator-4.0.10-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/docutils-0.13.1-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/feedparser-5.2.1-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/gevent-1.1.2-py2.7-linux-x86_64.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/Jinja2-2.8-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/lxml-3.6.4-py2.7-linux-x86_64.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/Mako-1.0.4-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/mock-2.0.0-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/passlib-1.6.5-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/Pillow-3.4.1-py2.7-linux-x86_64.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/psutil-4.3.1-py2.7-linux-x86_64.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/psycogreen-1.0-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/psycopg2-2.6.2-py2.7-linux-x86_64.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/Python_Chart-1.39-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/pydot-1.2.3-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/pyparsing-2.1.10-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/pyserial-3.1.1-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/python_dateutil-2.5.3-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/python_ldap-2.4.27-py2.7-linux-x86_64.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/python_openid-2.2.5-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/pytz-2016.7-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/pyusb-1.0.0-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/PyYAML-3.12-py2.7-linux-x86_64.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/qrcode-5.3-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/reportlab-3.3.0-py2.7-linux-x86_64.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/requests-2.11.1-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/suds_jurko-0.6-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/vatnumber-1.2-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/vobject-0.9.3-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/Werkzeug-0.11.11-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/XlsxWriter-0.9.3-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/xlwt-1.1.2-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/parts/odoo',
  '/opt/openerp/xayoni/appserver-xy/eggs/gunicorn-19.6.0-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/python_stdnum-1.5-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/pip-9.0.1-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/six-1.10.0-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/beautifulsoup4-4.5.3-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/pbr-1.10.0-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/funcsigs-1.0.2-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/MarkupSafe-0.23-py2.7-linux-x86_64.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/greenlet-0.4.10-py2.7-linux-x86_64.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/zc.buildout-2.8.0-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/zc.recipe.egg-2.0.2-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/Unidecode-0.4.20-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/uritemplate-3.0.0-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/oauth2client-4.0.0-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/httplib2-0.10.3-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/appdirs-1.4.0-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/packaging-16.8-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/rsa-3.4.2-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/pyasn1_modules-0.0.8-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/eggs/pyasn1-0.2.2-py2.7.egg',
  '/opt/openerp/xayoni/appserver-xy/celery_long_running_tasks',
]

from odoo_exec_environment import *

def test_and_usage_example(config_file):
    
    odoo = OdooExecutionEnvironment(config_file)
    odoo.open_database()

    try:
        user_model = odoo.env['res.users']
        admin_obj = user_model.search([('login','=','tarzan')])
        #print(admin_obj.read())
        print(admin_obj.signature)
        admin_obj.signature = "autre chose"
        #print 5/0
    except:
        odoo.rollback()
        import traceback
        traceback.print_exc()
    else:
        odoo.commit()

    odoo.close_database()


if __name__ == '__main__':
    print "Starting basic test"
    test_and_usage_example('/opt/openerp/xayoni/appserver-xy/etc/openerp.cfg')
    #test_and_usage_example(None, '/opt/openerp/xayoni/appserver-xy')
    sys.exit(0)

    
