# -*- coding: utf-8 -*-
"""Inouk Recipe patch"""
import os
import logging
import subprocess
import argparse
from pkg_resources import Requirement, resource_filename, resource_exists, resource_listdir


def generate_celery_app_from_template(project_directory, celery_app_folder):
    """ Create celery_app module in celery_app_projet if it does not exists.
    Update it if it already exists.
    :param project_directory: Where to create the celery_app module. Must be a valid directory.
    :param celery_app_folder: Name of folder that contains celery_app module
    
    """
    # Create celery_app_folder only if it does not exist!
    template_file_name = resource_filename('inouk.odoo_celery', 'celery_app_template.tgz')
    tmp_template_folder_path = os.path.join(project_directory, 'celery_app_template')
    template_app_project = os.path.join(project_directory, celery_app_folder)
    
    if not os.path.exists(template_app_project):
        if os.path.exists(tmp_template_folder_path):
            os.remove(tmp_template_folder_path)
        ret_code = subprocess.call(['tar', '-xzf', template_file_name, '-C', project_directory])
        if ret_code:
            raise Exception("Failed to extract '%s' to '%s'." %  (template_file_name, template_app_project,))
        os.rename(tmp_template_folder_path, template_app_project)
    else:
        print "Celery App module '%s' already exists. Leaving untouched. Delete it, if you want to re-genarate it." % template_app_project
    return



def generate_celery_app():
    """
    """
    parser = argparse.ArgumentParser(description="Generate a default celery_app for use with 'odoo_celery'.")
    parser.add_argument('-d', '--directory-path', 
                        type=str,
                        default=".",
                        help="Path where 'celery_app_folder' will be created. Default is current folder.")
    parser.add_argument('-f', '--folder_name', 
                        type=str,
                        default='celery_app_project',
                        help="Name of folder in which 'celery_app' module will be created. Default='celery_app_project'.")
    args = parser.parse_args()
    project_directory = args.directory_path
    if not os.path.isabs(project_directory):
        project_directory = os.getcwd()
    print "Creating '%s' in '%s'." % (args.folder_name, project_directory)
    generate_celery_app_from_template(project_directory, args.folder_name)
