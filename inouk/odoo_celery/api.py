# -*- coding: utf-8 -*-
import sys
import datetime
import inspect
import logging

_logger = logging.getLogger(__name__)

CELERY_TASKS_MODULES = []



# '@celery_task' annotation
def celery_task(decorated_function):
    """ Register celery tasks for automatic injection in celery_includes """
    global CELERY_TASKS_MODULES
    if decorated_function.__module__ not in CELERY_TASKS_MODULES:
        CELERY_TASKS_MODULES += [decorated_function.__module__]
        _logger.debug("'%s' added to Celery tasks module list.", decorated_function.__module__)
    return decorated_function


