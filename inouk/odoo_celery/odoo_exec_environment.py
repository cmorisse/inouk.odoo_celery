import logging
import csv
import odoo
from odoo import sql_db
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.modules.registry import Registry
from odoo.tools.safe_eval import safe_eval

from odoo.service.server import load_server_wide_modules, preload_registries
from odoo.cli.server import check_root_user, check_postgres_user, report_configuration 

__all__ = ['OdooExecutionEnvironment']

_logger = logging.getLogger(__name__)


class OdooExecutionEnvironment(object):
    """ Allows to execute Odoo code outside of Odoo by providing an execution 
    environnement.
    
    Typical usage is:
    
    odoo = OdooExecutionEnvironment(odoo_config_file_path)
    odoo.open_database('test_oee')
    env = odoo.env  # env is a standard Odoo 10 odoo.api.Environment()
    
    user_obj = env['res.users'].search([('id','=',1])
    print user_obj.login  # browse
    print user_obj.read()  # ...
    user_obj.signature = "updated outside Odoo"
    
    odoo.commit()  # or rollback
    odoo.close_database()  # don't forget this one
    """
    odoo_config_file = None
    odoo_config = None
    
    without_demo = None
    database_name = None
    create_database = None
    database = None
    
    @classmethod
    def load_modules(cls, odoo_config_file):
        _logger.debug("load_modules()")
        check_root_user()
        check_postgres_user()
        
        if odoo_config_file is None:
            # TODO: Implement Odoo default config file handling
            raise NotImplementedError("Odoo default config file handling not (yet) implemented.")
        cls.odoo_config_file = odoo_config_file
        odoo.tools.config.parse_config(['-c', cls.odoo_config_file])        
        cls.odoo_config = odoo.tools.config

        report_configuration()

        if cls.odoo_config['init'] or cls.odoo_config['update']:
            _logger.critical("OdooExecutionEnvironment can't be used to update a database (--update, -u). Exiting.")
            sys.exit(128)
        if cls.odoo_config["translate_out"]:
            _logger.critical("OdooExecutionEnvironment can't be used to export translations (--i18n-export). Exiting.")
            sys.exit(128)
        if cls.odoo_config["translate_in"]:
            _logger.critical("OdooExecutionEnvironment can't be used to import translations (--i18n-import). Exiting.")
            sys.exit(128)
        if cls.odoo_config["stop_after_init"]:
            _logger.critical("OdooExecutionEnvironment unsupported parameter --stop-after-init. Exiting.")
            sys.exit(128)

        # We rely on celery to manage processes and workers.
        # The code we run is very close to a ThreadedServer
        if cls.odoo_config['workers']:
            _logger.warning("--workers parameter ignored. OdooExecutionEnvironment "
                            "is a single threaded environment.")
            odoo.multi_process = False

        # From odoo.service.server.py
        load_server_wide_modules()

    @classmethod        
    def open_database(cls, database_name=None, without_demo=None, create_database=True):
        """ Opens a database, creates a cursor and an odoo.api.Environment() 
        in self.env.
        :param database_name: name of the database to open
        :param user_id: id of the 'res.users' that will injected into 
                        odoo.api.Environment()
        :param without_demo: shall any created database include demo data. See
                             `create_database` constructor parameters.
        """
        _logger.debug("open_database()")
        if without_demo is not None:
            cls.without_demo = without_demo
            cls.odoo_config['without_demo'] = without_demo
        else:
            cls.without_demo = cls.odoo_config['without_demo']
            
        cls.database_name = database_name or cls.odoo_config.get('db_name', None)
        if not cls.database_name:
            raise ValueError("Missing `database_name` parameter. It is "
                             "required when `db_name`is not defined in Odoo" 
                             "config file.")
        cls.create_database = create_database
        if cls.create_database:
            try:
                odoo.service.db._create_empty_database(cls.database_name)
            except odoo.service.db.DatabaseExists:
                pass
        cls.database = odoo.sql_db.db_connect(cls.database_name)
        _logger.info("database '%s' opened", cls.database_name)
        cls.registry = Registry(cls.database_name)

    def close_database(self):
        _logger.debug("close_database()")
        odoo.sql_db.close_db(cls.database_name)
        # We don't touch cls.database_name so it can be re-opened
        cls.database = None

    def _environment_manage_enter(self):
        """Simulate entering of odoo.api.Environment.manage() context manager 
        but don't leave it.
        You can refer to :class:``odoo.api.Environment`` documentation.
        """
        _logger.debug("_environment_manage_enter()")
        env_manage_generator = odoo.api.Environment.manage
        self._environments_manage_context = env_manage_generator().gen
        self._environments_manage_context.next()

    def _environment_manage_leave(self):
        """Simulates leaving of odoo.api.Environment.manage() context manager."""
        _logger.debug("_environment_manage_leave()")
        try:
            self._environments_manage_context.next()
        except StopIteration:
            pass
        else:
            raise RuntimeError("Incorrect handling/behaviour of odoo.api."
                               "Environment.manage() context manager.")
        del self._environments_manage_context

    def __init__(self, user_id=1,  manage_transactions=True):
        """
        """
        _logger.debug("OdooExecutionEnvironment()")
        super(OdooExecutionEnvironment, self).__init__()

        if OdooExecutionEnvironment.odoo_config_file is None:
            OdooExecutionEnvironmen.load_modules()
        if OdooExecutionEnvironment.database_name is None:
            OdooExecutionEnvironment.open_database()

        self.manage_transactions = manage_transactions
        self.user_id = user_id
        self.task_cr = self.database.cursor()  # create a new cursor
        self._environment_manage_enter()  # Simulate odoo.api.Environment.manage() context manager enter.
        self.env = odoo.api.Environment(self.task_cr, 
                                        self.user_id,
                                        {})
    

    def commit(self):
        """ Calls cursor `commit' if OdooExecutionEnvironment has been created
        with manage_transactions=True (default).
        """
        _logger.debug("commit()")
        if self.manage_transactions:
            self.task_cr.commit()

    def rollback(self):
        """ Calls cursor `rollback' if OdooExecutionEnvironment has been created
        with manage_transactions=True (default).
        """
        _logger.debug("rollback()")
        if self.manage_transactions:
            self.task_cr.rollback()
    
    def release(self):
        _logger.debug("release()")
        self.task_cr.close()
        self.task_cr = None
        self._environment_manage_leave()  # Simulate odoo.api.Environment.manage() context manager enter.
        self.env = None


    def __del__(self):
        """ Close all databases connections."""
        _logger.debug("__del__() => Pool status=%s", odoo.sql_db._Pool)
        #odoo.sql_db.close_all()
    

