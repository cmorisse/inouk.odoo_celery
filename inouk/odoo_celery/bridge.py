# coding: utf-8
import os
import sys
import json
import logging

from kombu.serialization import register
from celery.signals import celeryd_init, celeryd_after_setup, task_prerun, task_postrun, task_failure, task_success
from celery import Task

import odoo
from odoo_exec_environment import OdooExecutionEnvironment

_logger = logging.getLogger(__name__)


@celeryd_after_setup.connect
def celeryd_after_setup(sender=None, instance=None, conf=None, **kwargs):
    """ Celery `celeryd_after_setup` signal handler. 
    Here we inject a new :class:`odoo_exec_environment.OdooExecutionEnvironment` 
    instance into celery `app`.
    :param sender: The celery `hostname` that launches the worker.
    """
    # if not hasattr(instance.app, 'odoo'):
    #     instance.app.odoo = OdooExecutionEnvironment(conf.get('odoo_config_file', None))
    #     _logger.info("celeryd_after_setup injected OdooExecutionEnvironment into celery `app`.")
    _logger.debug("celeryd_after_setup() parent_pid=%s, pid=%s", os.getppid(), os.getpid())
    
    pass

@task_prerun.connect
def task_prerun_handler(sender=None, task=None, **kwargs):
    """ Celery `task_prerun` signal handler. 
    See :meth:`odoo_exec_environment.OdooExecutionEnvironment.task_prerun` for
    Odoo specific processing.
    :param task: The task that will be executed.
    """
    _logger.debug("task_prerun_handler() parent_pid=%s, pid=%s", os.getppid(), os.getpid())
    if isinstance(task, OdooTask):
        # TODO: extract user from args and pass to env
        task.app.odoo = OdooExecutionEnvironment(user_id=1)  
        task.odoo_env = task.app.odoo.env

@task_success.connect
def task_success_handler(sender=None, **kwargs):
    """ Celery `task_success` signal handler. 
    See :meth:`odoo_exec_environment.OdooExecutionEnvironment.handle_task_success` for
    Odoo specific processing.
    :param sender: here `sender` is the `task` whose execution succeed.
    """
    _logger.debug("task_success_handler() parent_pid=%s, pid=%s", os.getppid(), os.getpid())
    if isinstance(sender, OdooTask):
        sender.app.odoo.commit()

@task_failure.connect
def task_failure_handler(sender=None, **kwargs):
    """ Celery `task_failure` signal handler. 
    See :meth:`odoo_exec_environment.OdooExecutionEnvironment.handle_task_failure` for
    Odoo specific processing.
    :param sender: here `sender` is the `task` whose execution succeed.
    """
    _logger.debug("task_failure_handler() parent_pid=%s, pid=%s", os.getppid(), os.getpid())
    if isinstance(sender, OdooTask):
        sender.app.odoo.rollback()

@task_postrun.connect
def task_postrun_handler(sender=None, task=None, **kwargs):
    """ Celery `task_postrun` signal handler. 
    See :meth:`odoo_exec_environment.OdooExecutionEnvironment.task_postrun` for
    Odoo specific processing.
    :param task: The task that has finished.
    """
    _logger.debug("task_postrun_handler() parent_pid=%s, pid=%s", os.getppid(), os.getpid())
    if isinstance(task, OdooTask):
        task.app.odoo.release()

class OdooTask(Task):
    """Dummy Class used to trigger Odoo specific processing on tasks.""" 
    def __call__(self, *args, **kwargs):
        _logger.debug("OdooTask::__call__() parent_pid=%s, pid=%s", os.getppid(), os.getpid())
        if hasattr(self, 'odoo_env'):
            args = OdooModelWrapper.unwrap_odoo_model(self.odoo_env, args)
            kwargs = OdooModelWrapper.unwrap_odoo_model(self.odoo_env, kwargs)
        return self.run(*args, **kwargs)

#
# Encoding, Decoding of parameters of type Odoo Objects
#
class OdooModelWrapper:
    """ A thin wrapper for Odoo model objects """
    def __init__(self, obj):
        if obj['__CeleryOdoo_type__'] == 'odoo.models.BaseModel':
            self.type = 'odoo.models.BaseModel'
            self.model = obj['model']
            self.method_name = None
            self.ids = obj['ids']
        elif obj['__CeleryOdoo_type__'] == 'odoo.api.Environment':
            self.type = 'odoo.api.Environment'
            self.model = 'odoo.api.Environment'
            self.method_name = None
            self.ids = None
        else:
            raise NotImplementedError("Wrapping of '%s' not implemented." 
                                      % obj['__CeleryOdoo_type__'] )

    def __str__(self):
        return "OdooModelWrapper::%s(%s)" % (self.model, self.ids or '',)

    @classmethod
    def unwrap_odoo_model(cls, env, obj):
        """ Returns an object graph where wrappers have been replaced by Odoo 
        objects.
        """
        if isinstance(obj, OdooModelWrapper):
            if obj.type == 'odoo.models.BaseModel':
                odoo_obj = env[obj.model].search([('id', 'in', obj.ids)])
                return odoo_obj
            elif obj.type == 'odoo.api.Environment':
                return env
            elif obj.type == 'method':
                return getattr(odoo_obj, obj.method_name)
            else:
                raise UserError("Unwrap of Odoo '%s' not implemented." % obj.type)
        if isinstance(obj, (list, tuple,)):
            return map(lambda elem: cls.unwrap_odoo_model(env, elem), obj)
        if isinstance(obj, dict):
            patch = [(key, cls.unwrap_odoo_model(env, value),) for key, value in obj.iteritems()]
            obj.update(patch)
        return obj
    

class CeleryOdooEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, odoo.models.BaseModel):
             return {
                '__CeleryOdoo_type__': 'odoo.models.BaseModel', 
                'model': obj.__class__._name,
                'method_name': None,
                'ids': list(obj._ids),
            }
        elif isinstance(obj, odoo.api.Environment):
             return {
                '__CeleryOdoo_type__': 'odoo.api.Environment', 
                'model': None,
                'method_name': None,
                'ids': None,
            }
        return json.JSONEncoder.default(self, obj)

def celery_odoo_decoder(obj):
    if '__CeleryOdoo_type__' in obj:
        if obj['__CeleryOdoo_type__'] in ('odoo.models.BaseModel', 'odoo.api.Environment',):
            return OdooModelWrapper(obj)
        raise NotImplementedError("Decoding of '%s' not implemented." % obj['__CeleryOdoo_type__'] )
    return obj
    
# Encoder function      
def celery_odoo_dumps(obj):
    return json.dumps(obj, cls=CeleryOdooEncoder)

# Decoder function
def celerey_odoo_loads(obj):
    return json.loads(obj, object_hook=celery_odoo_decoder)

register('celery_odoo_json_serializer', 
         celery_odoo_dumps, 
         celerey_odoo_loads, 
         content_type='application/x-celery-odoo-json',
         content_encoding='utf-8')
