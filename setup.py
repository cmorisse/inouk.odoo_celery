# -*- coding: utf-8 -*-
"""
Inouk Odoo Celery allows to use Celery to manage job queues in Odoo.
"""
import os
from setuptools import setup, find_packages

name = "inouk.odoo_celery"
version = '0.1'

long_description = (
    '\nDetailed Documentation\n'
    '======================\n'
    + '\n' +
    open('README.txt').read()
    + '\n' +
    'Contributors\n'
    '============\n'
    + '\n' +
    open('CONTRIBUTORS.txt').read()
    + '\n' +
    'Change history\n'
    '==============\n'
    + '\n' +
    open('CHANGES.txt').read()
    + '\n'
)


setup(
    name=name,
    version=version,
    description="A buildout recipe to use celery with Odoo",
    long_description=long_description,
    
    # Get more strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Framework :: Buildout',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: GNU Library or Lesser General Public License (LGPL)',
        'Programming Language :: Python',
    ],
    keywords='odoo celery buildout',
    author=('Cyril MORISSE'),
    author_email='cmorisse at boxes3.net',
    url='https://bitbucket.org/cmorisse/inouk.odoo_celery.git',
    license='LGPL',
    packages=find_packages(), 
    namespace_packages=['inouk'],
    include_package_data=True,
    zip_safe=False,
    install_requires=['setuptools',],
    entry_points = {
        'zc.buildout': ["default = %s:Recipe" % name],
        'zc.buildout.uninstall': ["default = %s:uninstall_recipe" % name],
        'console_scripts': [
            'ioc_celery_launcher = inouk.odoo_celery.buildout_script:entry_point',
            'ioc_new_celery_app = inouk.odoo_celery.scripts:generate_celery_app',
        ],
    },
)


